# -*- coding: utf-8 -*-

class Numbering (object):
 
    def __init__ (self,v=None):
        if v is None:
            self.reset()
        else:
            self.set(self.convert(v))

    def set (self,index):
        self.current=index

    @property
    def first (self):
        return 1

    def reset (self):
        self.set(self.first)

    def __iter__ (self):
        return self

    def __next__ (self):
        s = str(self)
        self.incr()
        return s

    def next (self,inc = 1,index= None):
        if index is None:
            index = self.current
        return index + inc

    def prev (self,dec = 1,index= None):
        if index is None:
            index = self.current
        return max(self.first,index-dec)
    
    def format (self,index):
        return str(index)

    def incr (self,inc=1):
        self.current = self.next(inc=inc)
        
    def decr (self,dec=1):
        self.current = self.prev(dec=dec)

    def __str__(self):
        return self.format(self.current)
    
    def convert (self, value):
        return int(value)

class ArabicNumbering (Numbering):

    pass

class LetterNumbering (Numbering):

    @property
    def first (self):
        return 0

    def  format (self,index):
        l = []
        while True:
            l.append(chr(ord('a')+(index % 26)))
            index //= 26
            if not index:
                break
        return "".join(l[::-1])
    
    def convert(self, value):
        v = 0
        for c in value:
            v *= 26
            v += ord(c) - ord('a')
        return v

class CapLetterNumbering (LetterNumbering):
    
    def format (self,index):
        return super().format(index).upper()
    
    def convert(self,value):
        return super().convert(value.lower())

class RomanNumbering (Numbering):

    RomanNumbers = ('I','II','III','IV','V','VI','VII','VIII','IX','X',
                    'XI','XII','XIII','XIV','XV','XVI','XVII','XVIII','XIX','XX'
                    'XXI','XXII','XXIII','XXIV','XXV','XXVI','XXVII','XXVIII','XXIX','XXX',
                    'XXXI','XXXII','XXXIII','XXXIV','XXXV','XXXVI','XXXVII','XXXVIII','XXXIX','XL',
                    'XLI','XLII','XLIII','XLIV','XLV','XLVI','XLVII','XLVIII','XLIX','L',
                    'LI','LII','LIII','LIV','LV','LVI','LVII','LVIII','LIX','LX',
                    'LXI','LXII','LXIII','LXIV','LXV','LXVI','LXVII','LXVIII','LXIX','LXX',
                    'LXXI','LXXII','LXXIII','LXXIV','LXXV','LXXVI','LXXVII','LXXVIII','LXXIX','LXXX',
                    'LXXXI','LXXXII','LXXXIII','LXXXIV','LXXXV','LXXXVI','LXXXVII','LXXXVIII','LXXXIX','XC',
                    'XCI','XCII','XCIII','XCIV','XCV','XCVI','XCVII','XCVIII','XCIX','C'
                    )

    @property
    def first (self):
        return 0

    def format (self,index):
        return self.RomanNumbers[index]

    def convert(self, value):
        try:
            return self.RomanNumbers.index(value)
        except:
            return 0
    
class SmallRomanNumbering (RomanNumbering):

    def format (self,index):
        return super().format(index).lower()
    
    def convert (self,value):
        return super().convert(value.upper())
    
class NumSeq:

    mapping = {
        'digit' : ArabicNumbering,
        'letter' : LetterNumbering,
        'LETTER' : CapLetterNumbering,
        'roman' : SmallRomanNumbering,
        'ROMAN' : RomanNumbering
    }

    def __init__ (self,components,model=None):
        self.comp =  [__class__.mapping[t](v=v) for t,v in components]
        self.level = len(self.comp)
        if not model:
            model = ('',['.'] * self.level)
        self.model_intro = model[0]
        if self.model_intro:
            self.model_intro += ' '
        self.model_post = model[1]
        self._backup = [self.current]

    def format (self,index):
        return self.model_intro + "".join(c.format(v) + p for c,v,p in zip(self.comp[:self.level],index,self.model_post))
    
    @property
    def current (self):
        return [c.current for c in self.comp[:self.level]]
    
    @property
    def active (self):
        return self.comp[self.level-1]
    
    def __str__(self) -> str:
        return self.format(self.current)
    
    def up (self):
        self.level = max(1,self.level-1)
        return self
        
    def down (self,reset=False):
        self.level = min(len(self.comp),self.level+1)
        if reset:
            self.active.reset()
        return self
        
    def next (self,inc=1):
        self.active.incr(inc=inc)
        return self
    
    def prev (self,dec=1):
        self.active.decr(dec=dec)
        return self
    
    def first (self):
        self.comp[self.level-1].reset()
        return self

    def restore (self):
        index = self._backup[-1]
        if len(self._backup) > 1:
            self._backup[-1].pop()
        for c,v in zip(self.comp,index):
            c.set(v)
        self.level = len(index)
        return self
            
    def save (self):
        self._backup.append(self.current)

        

