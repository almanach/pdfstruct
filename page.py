# -*- coding: utf-8 -*-
import logging
from collections import Counter
import pandas as pd
from utils import make_html
from patterns import PATTERNS
from marker import OtherIndex, Cell, SecIndex, Section
from line import Line, Blob

SECTION_RE = PATTERNS['section']
OTHER_RE = PATTERNS['other']

class Page ():

    def __init__ (self, pageid, raw,doc,prev=None,next=None):
        self.raw = raw
        self.id = pageid
        self.doc = doc
        self.page_number = None

        self.set_orientation()
        
        self.width = raw['width']
        self.height = raw['height']
        self.prev = prev
        self.next = next
        

        # we have an issue for landscape pages

        self.lines = []
        self.blobs = []
        for block in raw['blocks']:
            #bid = block['number']
            if block['type'] != 0:
                # remove non text blocks
                # but keep them in a separate list
                self.blobs.append(Blob(block))
                continue
            #logging.info(f"BLOCK type={block['type']} {list(block.keys())}")
            #for line in sorted(sorted(block['lines'],key=lambda l: l['bbox'][1]), key=lambda l: l['bbox'][0]):
            try:
                for line in sorted(
                                sorted( filter(lambda l: len(l['spans']) > 0, block['lines']),
                                   key=lambda l: l['bbox'][0]), 
                                key=lambda l: l['spans'][0]['origin'][1]
                                ):
                    content = "".join(span['text'] for span in line['spans']).strip()
                    if not content: continue    
                    line = Line("".join(content),raw=line,page=pageid,doc=None)
                    if self.lines and self.lines[-1].try_merge(line,doc=None):
                        pass
                    else:
                        self.add_line(line)
            except:
                logging.info(f"Pb page {pageid} block={block['lines']}")
        # check if double column
        double = self.is_double_column
        if double:
            logging.info(f"Page {self.id} in double column sep={double}")
        # check if the page is dense in indexes and try to fill gaps if necessary
        self.handle_dense_index_page()
        self.handle_dense_index_page(index=OtherIndex)
        # track potential tables
        self.detect_tables()
        # some lines not in potential tables could now be joined
        changed = False
        while True:
            for line in self.lines:
                next_line = line.next
                if next_line and line.potential_merge(next_line):
                    line.join_next()
                    changed = True
            if changed:
                self.handle_dense_index_page()
                self.handle_dense_index_page(index=OtherIndex)
                self.clean()
                changed = False
            else:
                break

    def set_orientation (self):
        orientations = Counter()
        for block in self.raw['blocks']:
            if block['type'] != 0: continue
            for line in block['lines']:
                dir = tuple(line['dir'])
                orientations[dir] += 1
        try:
            orientation = orientations.most_common(1)[0][0]
        except:
            # page with no textual content
            orientation = (0,1)
        logging.info(f'Page {self.id} main direction is {orientation}')
        if orientation == (0,-1):
            self.rotate()
            self.orientation =  'landscape'
        else:
            self.orientation =  'portrait'
        logging.info(f"Page {self.id} orientation is {self.orientation}")

    def rotate (self):
        raw = self.raw
        width = raw['width']
        height = raw['height']
        raw['width'] = height
        raw['height'] = width
        for block in raw['blocks']:
            if block['type'] != 0: continue
            for line in block['lines']:
                line['dir'] = [1,0]
                bbox = line['bbox']
                line['bbox'] = [height-bbox[3],bbox[0],height-bbox[1],bbox[2]]
                for span in line['spans']:
                    bbox = span['bbox']
                    span['bbox'] = [height-bbox[3],bbox[0],height-bbox[1],bbox[2]]
                    orig = span['origin']
                    span['origin'] = [height-orig[1],orig[0]]

    def add_line(self, line):
        line.page = self.id
        if self.lines:
            line.prev = self.lines[-1]
        elif self.prev and self.prev.lines:
            line.prev = self.prev.lines[-1]
        self.lines.append(line)
        if line.prev:
            line.prev.next = line

    @property
    def df (self):
        ''' returns info about lines as a panda dataframe '''
        return pd.DataFrame.from_records([[l,l.x0,l.x1,l.y0,l.y1,l.width,l.height,l.where] for l in self.lines],
                                            columns=['line','x0','x1','y0','y1','width','height','where']
                                        )

    @property
    def is_double_column (self):
        ''' check is the page is double column and return the x-separating value between the columns, otherwise return None'''
        df = self.df
        min_x0 = df.x0.min()
        max_x1 = df.x1.max()
        page_width = max_x1 - min_x0
        max_width = df.width.quantile(q=0.9)
        prev_line = self.lines[0].prev if self.lines else None
        def reassign_lines ():
            prev = prev_line
            for line in self.lines:
                line.prev = prev
                if prev:
                    prev.next = line
                prev = line
        #if self.orientation == 'portrait' and max_width < 0.5 * page_width and page_width > 0.8 * self.width:
        if max_width < 0.5 * page_width and page_width > 0.8 * self.width:
            sep = min_x0 + 0.5 * page_width
            col1 = df[df.x1 < sep]
            for l in col1.line:
                l.where = 'col1'
            col2 = df[df.x0 > sep]
            for l in col2.line:
                l.where = 'col2'
            other = df[~df.index.isin(col1.index.union(col2.index))]
            # other elements are usually header and footer parts
            # but may also include wide tables covering both columns
            # in that case we need to retrieve parts in col1 and col2 that are vertically aligned with these elements
            top = min(col1.y0.min(), col2.y0.min())
            bottom = max(col1.y1.max(),col2.y1.max())
            middle_other = other[(other.y0 > top) & (other.y1 < bottom)]
            middle_y0 = []
            for y0,_ in middle_other.groupby('y0'):
                middle_y0.append(y0)
            logging.info(f"Page {self.id} middle other {middle_y0}")
            range_y0 = []
            for y0 in sorted(middle_y0):
                if not range_y0 or y0 > range_y0[-1][-1] + 40:
                    range_y0.append((y0,y0))
                else:
                    range_y0[-1] = (range_y0[-1][0],y0)
            range_y0 = [r for r in range_y0 if r[1] > r[0]]
            logging.info(f"Page {self.id} middle other range {range_y0}")
            displace = []
            for i,row in col1.iterrows():
                if any(r[0] <= row.y0 <= r[1] for r in range_y0):
                    displace.append(i)
            for i,row in col2.iterrows():
                if any(r[0] <= row.y0 <= r[1] for r in range_y0):
                    displace.append(i)
            if displace:
                logging.info(f"Page {self.id} middle other displace {displace}")
                for i in displace:
                    logging.info(f"\t{df.loc[i].line}")
                    df.loc[i].line.where=None
                col1 = col1[~col1.index.isin(displace)]
                col2 = col2[~col2.index.isin(displace)]
                other = pd.concat([other,df[df.index.isin(displace)]])
            col2 = pd.concat([col2,other[other.y0 > bottom]])
            col1 = pd.concat([col1,other[other.y0 < bottom]])
            self.lines = []
            def extend (df2):
                if len(df2):
                    self.lines.extend(list(df2.sort_values(by=['y0','x0']).line))
            extend(col1)
            extend(col2)
            reassign_lines()
            return sep
        else:
            self.lines = list(df.sort_values(by=['y0','x0']).line)
            reassign_lines()
            return None

    def detect_tables (self):
        df = self.df
        min_x0 = df.x0.min()
        max_x1 = df.x1.max()
        min_bin = 0
        max_bin = max(80,max(len(l.content) for l in self.lines)) if self.lines else 80
        delta = (max_x1 - min_x0) / max_bin  # bin size assuming no more than 40 chars
        logging.info(f"Page {self.id} max bin {max_bin} {delta=}")
        def holes2cols (holes):
            cols = set(range(min_bin,max_bin+1)).difference(holes)
            starts = []
            ends = []
            for x in sorted(cols):
                if ends and x == ends[-1]+1:
                    ends[-1] = x
                else:
                    starts.append(x)
                    ends.append(x)
            return list(zip(starts,ends))
        def col_intersect (cols1,cols2):
            mask = {}
            for _id,(u1,v1) in enumerate(cols1):
                for i in range(u1,v1+1):
                    mask[i] = _id
            for (u2,v2) in cols2:
                masks = [mask[i] for i in range(u2,v2+1) if i in mask]
                if len(set(masks)) > 1:
                    return True
            return False
        for g,df2 in df.groupby(by='where',dropna=False):
            logging.info(f'Page {self.id} processing {g}')
            all_holes = []
            potential_tables = {}
            for i,row in df2.iterrows():
                line = df2.loc[i].line
                if line.kind and type(line.kind) is not Cell:
                    all_holes = []
                    continue
                bin_x0 = int((row.x0-min_x0) / delta)
                bin_x1 = int((row.x1-min_x0) / delta)
                holes = set(range(min_bin,bin_x0)).union(range(bin_x1+1,max_bin+1))
                cols = holes2cols(holes)
                old_size = len(all_holes)
                all_holes = [xholes for xholes in all_holes if not col_intersect(holes2cols(xholes[1]),cols)]
                all_holes = [(j,_holes.intersection(holes)) for j,_holes in all_holes]
                all_holes = [xholes for xholes in all_holes if len(holes2cols(xholes[1])) > 1]
                all_cols = [holes2cols(_holes) for _,_holes in all_holes]
                if old_size - len(all_holes) > 6:
                    # a drastic size reduction in the number of potential cells in a table may indicate that we have reached the end
                    all_holes = []
                all_holes.append((i,holes))
                if False and len(all_holes) > 1:
                    logging.info(f"line {i} {df2.loc[i].line} : cols={holes2cols(holes)} all_cols={all_cols[-5:]}")
                    pass
                if len(all_holes) > 3:
                    logging.info(f"add table lines #={len(all_holes)} {i} {df2.loc[i].line} cols={cols} all_cols={all_cols}")
                    for j,_holes in all_holes:
                        potential_tables[j] = _holes
            prev=None
            for i in sorted(potential_tables):
                if not prev or i > prev + 1:
                    # starting a new table: need additionalchecks
                    j = i+1
                    line_i = df.loc[i].line
                    try:
                        line_j = df.loc[j].line
                        if line_j.y0 > line_i.y1 + 10:
                            continue
                    except:
                        # line_i is maybe the last one in the page !
                        continue
                cols=holes2cols(potential_tables[i])
                logging.info(f"page {self.id} line {i} in table cols={cols}")
                cell = df.loc[i].line
                bin_x0 = int((cell.x0-min_x0) / delta)
                bin_x1 = int((cell.x1-min_x0) / delta)
                holes = set(range(min_bin,bin_x0)).union(range(bin_x1+1,max_bin+1))
                cols = holes2cols(holes)
                cell.kind = Cell(cols=cols)
                prev = i

    def is_tdm_page (self,index=SecIndex):
        nlines = len(self.lines)
        indexes = list(self.collect(kind=index))
        return nlines and (len(indexes) > 10 or len(indexes) > 0.7 * nlines)
    
    def handle_dense_index_page(self,index=SecIndex):
        ''' handle page dense with indexes, looking for gaps to be corrected (should be rare)'''
        nlines = len(self.lines)
        indexes = list(self.collect(kind=index))
        #logging.info(f"handling potential dense index page #indexes={len(indexes)} #lines={nlines}")
        if nlines and (len(indexes) > 10 or len(indexes) > 0.7 * nlines):
            logging.info(f"Page {self.id} handling dense page for index={index}")
            for line in indexes[:-1]:
                gap = []
                next_line = line.next
                while next_line and type(next_line.kind) is not index:
                    gap.append(next_line)
                    next_line = next_line.next
                if gap:
                    content = " ".join(l.content for l in gap)
                    logging.info(f"Page {self.id} gap in indexes for #lines={len(gap)} content={content}")
                    # TO BE COMPLETED
                    for i,xline in enumerate(gap):
                        if type(xline.kind) is index: continue
                        if index == SecIndex:
                            # badly formatted index line
                            m = SECTION_RE.match(xline.content)
                            if m and m.group('page'):
                                xline.kind = SecIndex(m)
                                logging.info(f'Page {self.id} switched line {xline} to index')
                                continue
                            # multi-line index entry (3-lines)
                            if type(xline.kind) is Section:
                                next_line = xline.next
                                content = xline.content
                                merge = []
                                for j in range(i+1, len(gap)):
                                    yline = gap[j]
                                    if type(yline.kind) in {Section,SecIndex}:
                                        break
                                    content += ' ' + yline.content
                                    merge.append(yline)
                                    m = SECTION_RE.match(content)
                                    if m and m.group('page'):
                                        for _ in merge:
                                            xline.join_next()
                                        break

    def collect (self,kind=Section):
        return (l for l in self.lines if type(l.kind) == kind)
    
    @property
    def sections (self):
        return self.collect(kind=Section)
    
    @property
    def tdm (self):
        return self.collect(kind=SecIndex)

    def clean (self):
        ''' Removed deleted lines '''
        self.lines = [line for line in self.lines if line.prev or line.next]
        
    def display (self,start=None,end=None):
        if start is None:
            start = 0
        if end is None:
            end = len(self.lines)
        max_chars = 120
        rows = []
        def rows_display(rows):
            max_col_offset = max(line.kind.cols[-1][1] for row in rows for line in row)
            min_col_offset = min(line.kind.cols[-1][0] for row in rows for line in row)
            if max_col_offset == min_col_offset:
                max_col_offset += 10
            #logging.info(f"TABLE {min_col_offset=} {max_col_offset=}")
            def char_offset (col_offset):
                return (col_offset-min_col_offset) * max_chars / (max_col_offset-min_col_offset)
            def start (line):
                return char_offset(line.kind.cols[-1][0])
            def end (line):
                return char_offset(line.kind.cols[-1][1])
            for row in rows:
                row = sorted(row, key=lambda line: line.kind.cols[-1][0])
                starts = [start(line) for line in row] + [max_chars]
                ends = [0] + [end(line) for line in row] 
                delta = [int((s-e)/2) for s,e in zip(starts,ends)]
                #logging.info(f"{starts=} {ends=} {delta=}")
                txt = "".join(f"|{' '*d}{line.display()}{' '*d}|" for d,line in zip(delta,row))
                print(f"[{row[0].where}] {txt}")
        for i in range(start,min(end,len(self.lines))):
            line = self.lines[i]
            if type(line.kind) is Cell:
                if rows and abs(rows[-1][-1].base_y - line.base_y) < 5:
                    rows[-1].append(line)
                else:
                    rows.append([line])
            else:
                if rows:
                    rows_display(rows)
                    rows = []
                print(line)
        if rows:
            rows_display(rows)
            

    def display_html (self,start=None,end=None):
        if start is None:
            start = 0
        if end is None:
            end = len(self.lines)
        max_chars = 120
        rows = []
        text = []
        html_display = []
        def rows_display_html(rows):
            max_col_offset = max(line.kind.cols[-1][1] for row in rows for line in row)
            min_col_offset = min(line.kind.cols[-1][0] for row in rows for line in row)
            #logging.info(f"TABLE {min_col_offset=} {max_col_offset=}")
            def char_offset (col_offset):
                return (col_offset-min_col_offset) * max_chars / (max_col_offset-min_col_offset)
            def start (line):
                return char_offset(line.kind.cols[-1][0])
            def end (line):
                return char_offset(line.kind.cols[-1][1])
            #print(rows)
            trs= []
            for row in rows:
                row = sorted(row, key=lambda line: line.kind.cols[-1][0])
                #print(row)
                tds = "\n".join(line.display_html() for line in row)
                tr = make_html(tag="tr", text=tds, parent=True)
                trs.append(tr)
            trs = "\n".join(trs)
            table = make_html(text=trs, tag="table", attval=[("border", "1")], parent=True)
            #print(table)
            html_display.append(table)
                
        def text_display(texts):
            text = " ".join(t for t in texts)
            p = make_html(tag="p",text=text)
            #print(p)
            html_display.append(p)
        
        stop = min(end,len(self.lines))
        for i in range(start,stop):
            line = self.lines[i]
            
            if type(line.kind) is Cell:
                if text:
                    text_display(text)
                    text = []
                if rows and abs(rows[-1][-1].base_y - line.base_y) < 5:
                    rows[-1].append(line)
                else:
                    rows.append([line])
            elif line.kind is None:
                if rows :
                    rows_display_html(rows)
                    rows = []
                #print(line)
                if text == []:
                    c=i
                    next_line = line.next
                    current_line = line
                    text.append(line.content)
                    while next_line and next_line.kind is None and c <= stop-2:
                        #print(current_line,next_line)
                        text.append(next_line.content)
                        current_line = next_line
                        next_line = current_line.next
                        c+=1
                    #print(text)
            else:
                if rows :
                    rows_display_html(rows)
                    rows = []
                if text:
                    text_display(text)
                    text = []
                html_display.append(line.display_html())
                #print(line.display_html())
        if rows:
            rows_display_html(rows)
        if text :
            #print(text)
            text_display(text)
        footer_div = []
        
        if (self.id - 1) > -1 :
            href_1 = make_html(text = "Page précédente", tag="a",attval=[("href", f"./page_{str(self.id-1)}.html"), ("target","_self")])
        else:
            href_1 = make_html(text = "Page précédente", tag="a",attval=[("href", f"./page_{str(len(self.doc)-1)}.html"), ("target","_self")])
        subdiv = make_html(text=href_1, tag="div", attval=[("style", "position:absolute; left:0%; width:50%; height:100%;")], parent=True)
        footer_div.append(subdiv)
        if (self.id + 1 )< len(self.doc):
            href_2 = make_html(text = "Page suivante", tag="a",attval=[("href", f"./page_{str(self.id+1)}.html"), ("target","_self")])
        else:
            href_2 = make_html(text = "Page suivante", tag="a",attval=[("href", f"./page_{str(0)}.html"), ("target","_self")])
        subdiv = make_html(text=href_2, tag="div", attval=[("style", "position:absolute; left:50%; width:50%; height:100%;")], parent=True)
        footer_div.append(subdiv)
        footer_div = make_html(text="\n".join(footer_div), tag="div", attval=[("style","width:300px;height:100px;position:relative")], parent=True)
        html_display.append(footer_div)
        html_display = make_html(tag="html", text="\n".join(html_display), parent=True)
        print(html_display)
        return html_display