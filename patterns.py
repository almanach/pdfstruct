# -*- coding: utf-8 -*-
import regex as re
from numbering import RomanNumbering

def expand_patterns (patterns):
    ''' patterns is a dictionary of regexps 
    with {{<pattern_key>}} expressions to be replaced before compiling the regexps
    we assume thre is no reference cycle in the patterns 
    '''
    expanded = {}
    compiled = {}

    def expand_pattern (pattern,wrap=False):
        if pattern not in expanded:
            expanded[pattern] = re.sub(r'\{\{\s*(?P<key>\w+)\s*\}\}',
                                       lambda m: expand_pattern(m.group('key'),wrap=m),
                                       patterns[pattern])
        if wrap:
            return f"(?P<{pattern}>{expanded[pattern]})"
        else:
            return expanded[pattern]

    for pattern in patterns:
        compiled[pattern] = re.compile(expand_pattern(pattern),re.X)

    return compiled

PATTERNS = {
    'digit': r'[\d]{1,3}',
    'roman' : r'(?<![\p{L}])(?:' + "|".join(RomanNumbering.RomanNumbers).lower() + r')(?![\p{L}])',
    'ROMAN' : r'(?<![\p{L}])(?:' + "|".join(RomanNumbering.RomanNumbers) + r')(?![\p{L}])',
    'letter' : r'(?<![\p{L}])[a-z]{1,2}(?![\p{L}])',
    'LETTER' : r'(?<![\p{L}])[A-Z]{1,2}(?![\p{L}])',
    'num' : r'{{digit}}|{{roman}}|{{ROMAN}}|{{letter}}|{{LETTER}}',
    'post' : r'[.\p{Pd}/]{1,3}',
    'sep': r'[.:\p{Pd}/]{1,3}',
    'numbering' : r'''((?:
             {{num}}
             {{post}}
             (?P<space>\s?)
             )
             (?:
             {{num}}
             {{post}}
             (?P=space)
             )*
             )?   
             {{num}}
             {{post}}?
    ''',
    'page' : r'\d+',
    'dot' : r'[._\p{Pd}]*',
    'intro': r'\p{L}{3,}\.?',
    'title': r'\p{L}+.*?',

    'body': r'''
    {{numbering}}
    (?P<sep_spaces>
    \s?
    {{sep}}?
    \s*
    )
    {{title}}
    (:?\s*{{dot}}\s*{{page}})?
    \s*
    $
    ''',

    'section': r'''
    ^
    (?P<intro>(?i:section|partie|chapitre|annexe|appendix|appendice|volume)?)
    \s*
    {{body}}
    ''',

    'other': r'''
    ^
    {{intro}}
    \s*
    {{body}}
    '''
}

PATTERNS = expand_patterns(PATTERNS)
