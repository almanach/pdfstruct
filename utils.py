# -*- coding: utf-8 -*-
import logging

class ColorizedStreamHandler(logging.StreamHandler):

    color_map = {
        'black': 30,
        'red': 31,
        'green': 32,
        'yellow': 33,
        'blue': 34,
        'magenta': 35,
        'cyan': 36,
        'white': 37,
    }

    level_map = {
            logging.DEBUG: 'blue',
            logging.WARNING: 'yellow',
            logging.ERROR: 'cyan',
            logging.CRITICAL: 'red'
    }
    
    def format (self,record):
        message = logging.StreamHandler.format(self,record)
        logging.StreamHandler.format(self, record)
        parts = message.split('\n', 1)
        parts[0] = self.colorize(parts[0], record)
        message = '\n'.join(parts)
        return message

    def colorize (self,message,record):
        if record.levelno in self.level_map:
            fg = self.color_map[self.level_map[record.levelno]]
            message = f"\033[{fg}m{message}\033[0m"
        return message

def make_html(text, tag, attval=None, parent=None):
    """attval must be a list of tuples, text must be cleaned"""
    if parent is None:
        text = (
            text.replace("&", "&amp;").
            replace('"', "&quot;")
            .replace("'", "&apos;")
            .replace("<", "&lt;")
            .replace(">", "&gt;")
        )
    if attval is not None:
        atts = " ".join(['{}="{}"'.format(t[0], t[1]) for t in attval])
        ouv = "<{} {}>".format(tag, atts)
    else:
        ouv = "<{}>".format(tag)
    ferm = "</{}>".format(tag)
    annotated_text = "{}\n{}\n{}".format(ouv, text, ferm)
    return annotated_text

HTML_COLORS = { 91:"color:red;font-weight: bold;", 
               92:"color:green;font-weight:bold;", 
               93:"color:orange;font-weight:bold;", 
               94:"color:blue;font-weight:bold;", 
               95:"color:purple",
               36:"color:cyan;text decoration:underline;",
               96:"color:cyan;font-weight:bold;", "black":"black"
               }
