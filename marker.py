# -*- coding: utf-8 -*-
import regex as re
from unidecode import unidecode
from numbering import NumSeq

class Kind ():
    
    def __init__ (self):
        pass
    
    @property
    def color (self):
        return 'black'
    
    def display (self,content):
        return f"\033[{self.color}m{content}\033[0m"
    
class Section (Kind):
    
    def __init__(self,m):
        self.title = m.group('title')
        self.numbering = m.group('numbering')
        self.intro = m.group('intro')
        self.sep = m.group('sep')
        self.post = m.captures('post')
        self.nums = m.captures('num')
        self.numbering_span = m.span('numbering')
        self.title_span = m.span('title')

        start2info = {}
        for i,(s,e) in enumerate(m.spans('num')):
            start2info[s] = [s,e,self.nums[i]]
        for t in {'digit','letter','LETTER','roman','ROMAN'}:
            try:
                for s,e in m.spans(t):
                    if s in start2info:
                        start2info[s].append(t)
            except:
                pass

        post = self.post
        if len(post) < len(self.nums):
            post += [''] * (len(self.nums) - len(post))
            
        self.components = NumSeq([(x[3],x[2]) for x in start2info.values()],
                                 model = (self.intro,post)
                                 )
        #logging.info(f"components are {self.components}")

        model = ''
        if self.intro: model += self.intro + ' '
        s0,e0 = self.numbering_span
        numbering = self.numbering
        prev = s0
        for x in start2info.values():
            model += numbering[prev-s0:x[0]-s0] + f"<{x[3]}>"
            prev = x[1]
        model += numbering[prev-s0:]
        self.model = model

    @property
    def color (self):
        #return 'red'
        return 91
    
    @property
    def index (self):
        return (self.numbering,re.sub(r'[^\p{L}]','',unidecode(self.title.lower())))
    
    def display(self, content):
        start,end = self.numbering_span
        content = [content[0:start],'\033[4m' + content[start:end] + '\033[0m', content[end:]]
        content =  ''.join(super(__class__,self).display(c) for c in content)
        return content
    
    @property
    def level(self):
        return len(self.nums)
    
    @property
    def tag(self):
        return f"h{self.level}"
    
    
class SecIndex (Section):

    def __init__ (self,m):
        super().__init__(m)
        self.page = int(m.group('page'))

    @property
    def color (self):
        #return 'green'
        return 92

class Cell (Kind):

    def __init__(self,cols=None):
        self.cols = cols

    @property
    def color (self):
        return 93
    
    @property
    def tag (self):
        return "td"
    
class Header (Kind):
    
    @property
    def color (self):
        return 94
    
    @property
    def tag (self):
        return "header"

class Footer (Kind):
    
    @property
    def color (self):
        return 95
    
    @property
    def tag (self):
        return "footer"
    
class Other (Section):

    @property
    def color (self):
        return 36
    
    @property
    def tag (self):
        return "other"
    
class OtherIndex (SecIndex):
    
    @property
    def color (self):
        return 96