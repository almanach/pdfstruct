# -*- coding: utf-8 -*-
from document import Document

class Collection ():

    def __init__ (self,raw,page=None,n=None):
        if type(raw) is Document:
            self.docs = raw.split()
        elif type(raw) in (list, tuple) and type(raw[0]) is Document:
            self.docs = list(raw)
        else:
            self.docs = Document(raw,page=page,n=n).split()

    @classmethod
    def from_json(cls, filename, **kwargs):
        return cls(Document.from_json(filename, **kwargs))

    @classmethod   
    def from_pdf(cls,filename,**kwargs):
        return cls(Document.from_pdf(filename, **kwargs))

    @property
    def __len__ (self):
        return len(self.docs)

    def to_html (self,path="./pages", pdf=None):
        for i,doc in enumerate(self.docs):
            doc.to_html(path=f"{path}/doc_{i+1}",pdf=pdf)

    def display (self):
        for doc in self.docs:
            doc.display()

    def add_documents(self,doc):
        self.docs.append(doc)
