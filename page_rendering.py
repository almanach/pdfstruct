import streamlit as st
import glob
import os
import base64
import streamlit.components.v1 as components

def get_link_template(pagename):
    template = f"./?page={pagename}"
    return template

def get_page_number(pagename):
    return int(pagename.replace("_", "").replace("page", ""))

def make_page_name(number):
    return f"page_{str(number)}"

def update_params():
    st.experimental_set_query_params(page=st.session_state.qp)
    st.session_state["page"] = st.session_state.qp
    

def get_page_names(pages):
    names = sorted([p.split("/")[-1].replace(".html", "") for p in pages])
    return names


def show_pdf(file_path):
    with open(file_path,"rb") as f:
        base64_pdf = base64.b64encode(f.read()).decode('utf-8')
    pdf_display = F'<embed src="data:application/pdf;base64,{base64_pdf}" width="600" height="800" type="application/pdf">'
    st.markdown(pdf_display, unsafe_allow_html=True)


def display_page(selection):
    def arrows(prev, nextp, navpage):
        if navpage:
            col1,col2 = st.columns([0.5,6])
            with col1:
                prev = make_page_name(prev)
                link = get_link_template(prev)
                st.markdown(f'<a href="{link}" target="_self">⬅️</a>', unsafe_allow_html=True)
            with col2:
                nextp = make_page_name(nextp)
                link = get_link_template(nextp)
                st.markdown(f'<a href="{link}" target="_self">➡️</a>', unsafe_allow_html=True)
    with open(f"pages/{selection}.html", "r", encoding="utf8", newline="\n") as inf:
        content = inf.read().replace("./page_", "./?page=page_").replace(".html", "")
        
    if selection !="overview":
        navpage = True
        pagenum = get_page_number(selection)
        nextp = pagenum + 1 if pagenum < (len(options)-2) else 1
        prev = pagenum -1 if pagenum > 1 else len(options)-2
        arrows(prev, nextp,navpage)          
    clean = selection.replace("_", " ")    
    st.markdown(f"## {clean}")
    tab1, tab2,tab3 = st.tabs(["HTML Code","HTML Rendering", "PDF Rendering"])
    tab1.code(content)
    tab2.write(content, unsafe_allow_html=True)
    pdf = f"./pages/{selection}.pdf"
    with tab3:
        if os.path.exists(pdf):
            show_pdf(pdf)
        else:
            st.write("pas de PDF pour cette page")
    if selection !="overview":
        arrows(prev, nextp, navpage)

pages = glob.glob("pages/*.html")
query_params = st.experimental_get_query_params()
options = get_page_names(pages)

index = 0 if "page" not in st.session_state else options.index(st.session_state["page"])
selection = st.sidebar.selectbox("Choisir une page", options=options,key="qp",on_change=update_params, index = index)

if "page" not in st.session_state:
    if query_params:
        st.session_state["page"] = query_params["page"][0]
    else:
        st.experimental_set_query_params(page=selection)
        st.session_state["page"] = selection
        
display_page(st.session_state["page"])
    
