# -*- coding: utf-8 -*-
import logging
import regex as re
from utils import make_html, HTML_COLORS
from patterns import PATTERNS
from marker import Section, SecIndex, Other, OtherIndex

SECTION_RE = PATTERNS['section']
OTHER_RE = PATTERNS['other']

class Blob ():
    
    def __init__(self,raw):
        self.bbox = raw['bbox']
        self.ext = raw['ext'] if 'ext' in raw else None
        self.width = raw['width']
        self.height = raw['height']
        
    @property
    def x0 (self):
        return self.bbox[0]

    @property
    def y0 (self):
        return self.bbox[1]

    @property
    def x1 (self):
        return self.bbox[2]

    @property
    def y1 (self):
        return self.bbox[3]
        
class Line ():

    def __init__ (self,content,raw,page=None,doc=None):
        #content = re.sub(r'[●••·◘○◙‣⁃⁌⁍■□∙]','*',content)
        self.content = content
        self.raw = raw
        self.where = None
        self.kind = None
        self.prev = None
        self.next = None
        self.page = page
        self.update(doc=doc)
        
    def update(self,doc=None):
        self.is_potential_section(doc=doc)
        self.is_potential_other_index()

    def __str__ (self):
        content = self.content
        if self.kind:
            content = self.kind.display(content)
        return f"[{self.where}] {content}"
    
    def display (self):
        content = self.content
        if self.kind:
            content = self.kind.display(content)
        return content
    
    def display_html(self):
        content = self.content
        if self.kind:
            #content = colored( content,self.kind.color)
            tag = self.kind.tag
            color = HTML_COLORS[self.kind.color]
            styles = [("style",color)]
        else:
            tag = "p"
            styles = [("style","color:black;")]
        tagged =  make_html(content, tag, attval=styles)
        return tagged

    @property
    def bbox (self):
        return self.raw['bbox']

    @property
    def origin (self):
        return self.raw['spans'][0]['origin'] 
    
    @property
    def base_y (self):
        return self.origin[1]

    @property
    def x0 (self):
        return self.bbox[0]

    @property
    def y0 (self):
        return self.bbox[1]

    @property
    def x1 (self):
        return self.bbox[2]

    @property
    def y1 (self):
        return self.bbox[3]

    @property
    def width (self):
        return self.x1-self.x0

    @property
    def height (self):
        return self.y1-self.y0
    
    @property
    def orientation (self):
        return self.raw['dir']
    
    def potential_merge (self,other,doc=None):
        #logging.info(f"test merge {self} with {other} : {self.base_y=}  {other.base_y=} {self.x1=} {other.x0=}")
        if abs(self.base_y - other.base_y) < 4 and self.x1 < other.x0 and (other.x0 < self.x1+10 or self.is_potential_section_aux(content=self.content + ' ' + other.content,doc=doc)) and self.height < 30:
            return True
        #logging.info('HERE0')
        # multi-line section
        if (type(self.kind) is Section 
            and (type(other.kind) is not SecIndex or not self.acceptable_numbering(other.kind.numbering))
            and re.search(r'[._\p{Pd}]{5,}\s*\d+$',other.content)):
        #if type(self.kind) is not SecIndex  and type(other.kind) is not SecIndex and re.search(r'[._\p{Pd}]{5,}\s*\d+$',other.content) and self.is_potential_section_aux(content=self.content + ' ' + other.content,doc=doc):
            #logging.info(f"MERGE other {other} kind={other.kind}")
            return True
        # multi-line section
        if (not self.kind 
            and other.kind not in {Section,SecIndex,OtherIndex}
            and re.search(r'[._\p{Pd}]{5,}\s*\d+$',other.content) and self.is_potential_other_aux(self.content + ' ' + other.content)):
            return True

    def try_merge(self,other,doc=None):
        if self.potential_merge(other,doc=doc):
            logging.info(f"*** Page {self.page} merging {self.bbox}:<{self.content}> with {other.bbox}:<{other.content}>")
            spaces = ' ' * max(1,int((len(self.content) + len(other.content)) / (self.width + other.width)))
            self.content += spaces + other.content
            self.bbox[2] = other.x1
            self.bbox[1] = min(self.y0, other.y0)
            self.bbox[3] = max(self.y1, other.y1)
            self.update()
            return True
        else:
            return False
        
    def acceptable_numbering (self,numbering):
        #logging.info(f"Page {self.page} test numbering {numbering}")
        if re.match(r'^(\w+[.\p{Pd}/])+\w+[.\p{Pd}/]?$',numbering):
            return True
        if re.match(r'^(\w+[.\p{Pd}/]\s+)+\w+[.\p{Pd}/]$',numbering):
            return True
        if re.match(r'^(\d{1,3}|[ivxl]{1,4}|[IVXL]{1,4})[.\p{Pd}/]$',numbering):
            return True
        return False
    
    def is_potential_section_aux (self,content=None,doc=None):
        if not content:
            content = self.content
        #logging.info(f'try potential section {self} content {content}')
        m = SECTION_RE.match(content)
        #logging.info(f"Page {self.page} test section on {content} => {m}")
        if m and (m.group('sep') or m.group('intro') or m.group('post') or self.acceptable_numbering(m.group('numbering'))):
            if not doc or doc.is_section_in_index(Section(m)):
                #logging.info(f"Page {self.page} potential section {content}")
                return m
        return None

    def is_potential_section(self,doc=None):
        ''' check if the line could be a section title, possibly with some constraints
            return the numbering if true otherwise None
        '''
        m = self.is_potential_section_aux()
        #if m:
        #    logging.info(f"got potential section {m.group('sep')=} {m.group('intro')=} {m.group('numbering')=} => {self.acceptable_numbering(m.group('numbering'))}")
        if m and (m.group('sep') or m.group('intro') or self.acceptable_numbering(m.group('numbering'))):
            if m.group('page') and m.group('dot') and len(m.group('dot')) > 2:
                # tdm
                logging.info(f"Page {self.page} TDM <{self.content}> intro={m.group('intro')} numbering={m.group('numbering')} sep={m.group('sep')} title={m.group('title')}")
                self.kind = SecIndex(m)
            elif not m.group('dot') and m.group('sep_spaces'):
                logging.info(f"Page {self.page} SECTION <{self.content}> intro={m.group('intro')} numbering={m.group('numbering')} sep={m.group('sep')} title={m.group('title')}")
                self.kind = Section(m)
        #logging.info(f"{self}")
                
    def is_potential_other_aux (self,content=None):
        if not content:
            content = self.content
        #logging.info(f"Page {self.page} test section on {content}")
        m = OTHER_RE.match(content)
        if m and not re.match(r'^[IVXLC]+\.?$',m.group('intro')) and m.group('page') and m.group('dot'):
            logging.info(f"Page {self.page} potential other index {content}")
            return m
        else:
            return None
        
    def check_with_other_index (self,index=None):
        content = self.content
        #logging.info(f"Page {self.page} test section on {content}")
        m = OTHER_RE.match(content)
        if (m 
            and m.group('intro').lower() == index.kind.intro.lower() 
            and m.group('numbering') == index.kind.numbering
           ):
            self.kind = Other(m)
            logging.info(f"Page {self.page} {self} caption for {index}")
            return m
        else:
            return None
        
    def is_potential_other_index (self,constraints=None):
        if self.kind:
            return
        m = self.is_potential_other_aux()
        if m and m.group('page'):
            self.kind = OtherIndex(m)

    def delete(self):
        if self.next:
            self.next.prev = self.prev
        if self.prev:
            self.prev.next = self.next
        self.prev=None
        self.next=None

    def join_next(self):
        next_line = self.next
        spaces = ' ' * max(1,int((len(self.content) + len(next_line.content)) / (self.width + next_line.width)))
        self.content += spaces + next_line.content
        # adjust bounding box
        self.bbox[2] = next_line.x1
        self.bbox[1] = min(self.y0, next_line.y0)
        self.bbox[3] = max(self.y1, next_line.y1)
        # adjust prev and next pointers
        self.next = next_line.next
        next_line.next = None
        next_line.prev = None
        self.update()
