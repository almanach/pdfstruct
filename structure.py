# -*- coding: utf-8 -*-
import logging
import sys
from utils import ColorizedStreamHandler
from collection import Collection

logging.basicConfig( handlers=[ColorizedStreamHandler(stream=sys.stdout)],
                    level=logging.DEBUG,
                    format='[%(asctime)s] %(message)s')

if __name__ == '__main__':
    logging.info(sys.argv)
    datafile = sys.argv[1] if len(sys.argv) > 1 else "../unprocessed/114591_FEI_dict.json"
    if datafile.endswith(".json"):
        doc=Collection.from_json(datafile)
    else:
        doc=Collection.from_pdf(datafile)
    doc.display()
    doc.to_html()

