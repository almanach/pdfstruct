import regex as re
from marker import Header, Footer
from itertools import groupby
import regex as re


def group_by_kind(section_lines):
   '''makes groups of continuous line kinds and deletes headers and footers'''
   paras = [{"kind": k, "lines":list(g)} for k,g in groupby(section_lines, key=lambda x : x.kind)]
   paras = [d for d in paras if type(d["kind"]) is not Footer and type(d["kind"]) is not Header]
   return paras

def split_para(lst,final_ponct):
   '''returns a group of lines where the last element is the smallest and ends with final punct'''
   paras = []
   para = []
   i = 0
   while i < len(lst)-1:
      line = lst[i]
      next_line = lst[i+1]
      para.append(line)
      if next_line.x1 >= line.x1:
         if i == len(lst)-2 :
               para.append(next_line)
               paras.append(para)     
      else: 
         if para and re.match(final_ponct, next_line.content[-1]):
               para.append(next_line)
               i += 1
               paras.append(para)
               para = []
      i += 1
   if i != len(lst):
      paras.append([lst[i]])
   return paras

def is_normal_para(para):
   check = False
   final_ponct = re.compile(r"([\.\?\!…])")
   caption = re.compile(r"(Figure|Carte|Table|Tableau|Schéma|Photo|Image) +\d+ *:")
   if re.match(r"\p{Lu}", para[0].content[0]) and re.match(final_ponct, para[-1].content[-1]):
      if not re.search(caption, para[0].content) and len(para[0].content.split(" ")) > 5:
         check = True
   return check

def clean_para(para):
   txt = "\n".join([l.content for l in para])
   ntxt = re.sub(r"’", r"'", txt)
   ntxt = re.sub(r"\.{3}", r"…", ntxt)
   ntxt = re.sub(r"([\p{Ll}&,;\(\)])-? *\n *([^\.\?\!…])", r"\1 \2", ntxt)
   ntxt = re.sub(r"([0-9]) *\n *(\p{Ll})", r"\1 \2", ntxt)
   ntxt = re.sub(r"\n *(\p{Ll})", r" \1", ntxt)
   ntxt = re.sub(r"(\n *){2,}", "", ntxt)
   ntxt = ntxt.rstrip()
   ntxt = " ".join(ntxt.split())
   return ntxt

def get_all_paras(paras):
   # returns a list of dicts for each paragraph. 
   # The dicts keys are the type of the paragraph ("normal" or "other") and the paragraph itself("para")
   all_paras = []
   for para in paras:
      if is_normal_para(para):
        all_paras.append({"type": "normal", "para":para})
      else:
         all_paras.append({"type": "other", "para":para})
   return all_paras

def filter_normal_paras(all_paras):
   #returns list of well-formed paragraph text
  normal_paras = [item["para"] for item in all_paras if item["type"] == "normal"]
  #print("found", len(normal_paras), "normal paragraphs")
  normal_paras = [clean_para(para) for para in normal_paras]
  return normal_paras

def find_paragraphs_in_block(block):
   final_ponct = re.compile(r"([\.\?\!…])")
   # group lines if they all begin at the same x0 coordinate
   # an end of paragraph line must have a smaller x1 and a final punct sign.
   # split those subgroups into smaller groups according to that
   #print(len(block["lines"]), "lines in this block")
   groupedx0 = [list(g) for k, g in groupby(block["lines"], key = lambda x: x.x0)]
   block_paras = []
   for lst in groupedx0:
      paras = split_para(lst, final_ponct)
      block_paras.append(get_all_paras(paras))
   return block_paras

