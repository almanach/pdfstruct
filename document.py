# -*- coding: utf-8 -*-
import logging
import json
import os
import regex as re
import numpy as np
from collections import Counter, defaultdict
from itertools import chain
import fitz
from utils import make_html, HTML_COLORS
from marker import Cell, Section, SecIndex, Other, OtherIndex, Header, Footer
from page import Page
from text_retrieval import group_by_kind, find_paragraphs_in_block, clean_para, filter_normal_paras

class Document ():

    def __init__ (self,raw,page=None,n=None):
        self._tdm = defaultdict(list)
        self._tdm_light = defaultdict(list)
        self.page_mapping = {}
        if type(raw) is list and type(raw[0]) is Page:
            self.pages = raw
            self.raw = [page.raw for page in self.pages]
        else:
            self.raw = raw
            self.pages = []   
            if page :
                if not n:
                    n = 1
                for i in range(page,page+n):
                    self.add_page(self.raw[i])
            else:
                for s in self.raw:
                    self.add_page(s)
        
    def single_doc_process (self):
        # the following methods should be called after sub-document detection
        # (after building a collection of documents)
        logging.info(f'completing processing single document')
        self.detect_header_and_footer()
        self.check_and_filter_sections()
        self.filter_other_indexes()
        logging.info(f'done processing single document')
        return self
    
    def sub_doc(self,start=0,end=None):
        if not end:
            end = len(self.pages)
        logging.info(f"Potential sub-document between pages {start} and {end}")
        pages = self.pages[start:end]
        for i,page in enumerate(pages):
            page.id = i
            for line in page.lines:
                line.page = i
        return Document(pages)

    def split (self):
        ''' split a multi-part document into a collection of documents'''

        # change + reset of page numbering in headers or footers could be used
        numbering_ranges = self.detect_page_numbering()
        if len(numbering_ranges) > 1:
            docs = []
            for start,end in numbering_ranges:
                
                potential_tdm = []
                prev_tdm = None
                lpages=set([])
                for page in self.pages[start:end+1]:
                    if page.is_tdm_page():
                        if not prev_tdm:
                            potential_tdm.append(page.id)
                        prev_tdm = page
                        for line in page.tdm:
                            lpages.add(line.kind.page)
                    else:
                        prev_tdm = None
                if potential_tdm:
                    lpages = sorted(list(lpages))
                    lpage_max = lpages[0]
                    # sometimes a bad ToC entry is returned with a very large page number
                    # we check that the max page number is close from the other page numbers
                    for lpage in lpages[1:]:
                        if lpage > lpage_max + 20:
                            break
                        lpage_max = lpage
                    if len(potential_tdm) > 1:
                        logging.warning(f"Several Tables of Contents within page range {start} {end}")
                    logging.info(f"found ToC at page {potential_tdm[-1]} in page range {start} {end} with last reference on page {start+lpage_max-1}")
                    if start + lpage_max > end + 1:
                        logging.warning(f"Table of content entry for logical page={lpage_max} outside of physical page range {start} {end}")
                        # should extend the range of the sub-document if necessary
                        # specially if the next one was a gap filler
                else:
                    logging.warning(f"No Table of Content within page range {start} {end}")
                docs.append(self.sub_doc(start=start,end=end+1))
            return [doc.single_doc_process() for doc in docs]
        
        logging.info(f"single document: no splitting")
        return [self.single_doc_process()]

    @classmethod
    def from_json(cls, filename, **kwargs):
        with open(filename, 'r') as f:
            return cls(json.load(f),**kwargs)
        
    @classmethod   
    def from_pdf(cls,filename, extract_pages=None,path=None,**kwargs):
        ## should cache the json
        def get_pages(doc, path, kwargs):
            page, n = kwargs["page"], kwargs["n"]
            if path is None:
                path = "./pages"
            if not os.path.exists(path):
                os.makedirs(path)
            for i in range(page,page+n):
                out = fitz.open()
                out.insert_pdf(doc, from_page=i, to_page=i)
                out.save(f"{path}/page_{i}.pdf")

        doc = fitz.open(filename)
        jsn = []
        for i, page in enumerate(doc):
            jsn.append(json.loads(page.get_text("json")))
        if extract_pages:
            get_pages(doc,path,kwargs)
        return cls(jsn, **kwargs)

    def add_page(self,content):
        page = Page(len(self),content,prev=self.pages[-1] if self.pages else None,doc = self)
        self.pages.append(page)
        self.process_indexes(page.tdm)

    def __len__ (self):
        return len(self.pages)
    
    def collect (self,kind=Section):
        return chain.from_iterable((p.collect(kind=kind) for p in self.pages))
    
    @property
    def sections (self):
        return self.collect(kind=Section)
    
    @property
    def tdm (self):
        return self.collect(kind=SecIndex)
    
    @property
    def captions (self):
        return self.collect(kind=Other)
    
    @property
    def caption_tdm (self):
        return self.collect(kind=OtherIndex)

    @property
    def cells (self):
        return self.collect(kind=Cell)
    
    @property
    def lpage (self,n):
        return self.page[self.page_mapping[n]] if n in self.page_mapping else None
    
    def detect_page_numbering (self):
        entries = defaultdict(list)
        
        def normalize (line):
            content = line.content
            nums = [int(x) for x in re.findall(r'(\d+)',content)]
            content = re.sub(r'\d+',r'<<NUM>>',content)
            content = re.sub(r'\s+',r'',content)
            content = content.lower()
            y = int(line.y0)
            return ((y,content),nums)
        
        def is_range (info,key=None,line=None):
            #logging.info(f"entering is_range {key=} {line=} {info=}")
            orig = prev = info[-1]
            prev_deltas = [prev[0] - n for n in prev[1]]
            where = None
            shift = None
            n = 1
            for x in info[:-1][::-1]:
                deltas = [x[0] - n for n in x[1]]
                if x[0] == prev[0]:
                    break
                if len(deltas) != len(prev_deltas):
                    break
                if False and where is None and prev[0] > x[0] + 3:
                   break
                if where is None:
                    for i in range(0, len(deltas)):
                        if deltas[i] == prev_deltas[i]:
                            where = i
                            shift = deltas[i]
                            break
                if (where is None or 
                    ( prev_deltas[where] != deltas[where]
                     # the following condition could deal with document insertion within a larger document
                     # however, need to maintain a list of shifts between physical and logical page numbers 
                     # and deltas[where] != prev_deltas[where] + x[0] - prev[0]
                    )):
                    break
                prev = x
                prev_deltas = deltas
                n += 1

            if n > 4:                
                #logging.info(f"range extension from {prev[0]} to {orig[0]} with key={key} where={where} shift={shift} info={info}")
                return (prev[0],orig[0],shift,where,key,n)
            else:
                #logging.info(f"no range extension")
                return None
        
        potential_range = {}
        def test_line (line):
            xkey,nums = normalize(line)
            #logging.info(f"Test footer {page.id} {line.y0=} {xkey=} {nums=} info={footers[xkey]}")
            if nums:
                entries[xkey].append((page.id,nums))
                info = entries[xkey]
                if len(info) > 4:
                    #logging.info(f"Try range {page.id} {line.y0=} {xkey=} {nums=} {line=}")
                    xrange = is_range(info,key=xkey,line=line)
                    if xrange:
                        #logging.info(f"range extension from {info[0][0]} to {page.id} with footer {xkey}")
                        potential_range[(xkey,xrange[0])] = xrange
        
        for page in self.pages:
            height = page.height
            #logging.info(f"Test page {page.id} {height=}")

            df = page.df.sort_values(by='y0')
            for line in list(df.head(10).line.values):
                if line.y0 > 0.33 * height: continue
                test_line(line)
                
            for line in list(df.tail(10).line.values):
                if line.y0 < 0.66 * height: continue
                test_line(line)
        
        ranges = list(potential_range.values())
        ranges = sorted(ranges,key = lambda x: x[0])
        xranges = []

        def handle_gap (start):
            if xranges and start > xranges[-1][1] + 1:
                gap = start - xranges[-1][1]
                logging.warning(f"Numbering gap of {gap} pages between pages {xranges[-1][1]} and {start}")
                if gap > 3:
                    # add new doc
                    xranges.append((xranges[-1][1],start-1))
                else:
                    # extend previous document
                    xranges[-1] = (xranges[-1][0],start-1)

        for xrange in ranges:
            xstart = xrange[0]
            xend = xrange[1]
            shift = xrange[2]
            start = shift+1
            x=(start,xend)
            if not xranges and start > 0:
                logging.warning(f"First pages 0 to {start-1} not in a numbering range")
                start = 0
                x=(start,xend)
            if not xranges or start > xranges[-1][1]:
                handle_gap(start)
                logging.info(f"Adding page range start={start} end={xend} xrange={xrange}")
                xranges.append(x)
            elif start == xranges[-1][0]:
                logging.info(f"Compatible overlapping numbering ranges {xranges[-1]} and {x} xrange={xrange}")
                xranges[-1] = (start,max(xend,xranges[-1][1]))
            else:
                logging.warning(f"Non-compatible overlapping numbering ranges {xranges[-1]} and {x} xrange={xrange}")
        # handle potentially final gap at the end of the document
        handle_gap(len(self.pages))
        return xranges

    def detect_header_and_footer (self):
        '''Detect headers and footers as topmost and bottommost lines stable modulo numbers'''
        npages = len(self.pages)
        
        if npages < 10:
            return
        
        headers = defaultdict(list)
        footers = defaultdict(list)
        
        def normalize (content):
            content = re.sub(r'\d+',r'<<NUM>>',content)
            content = re.sub(r'\s+',r' ',content)
            content = content.lower()
            return content
        
        for page in self.pages:
            df = page.df.sort_values(by='y0')
            for line in list(df.head(10).line.values):
                headers[normalize(line.content)].append(line)
            for line in list(df.tail(10).line.values):
                footers[normalize(line.content)].append(line)
        # we use a threshold of 0.45 because we may have different headers and footers for odd and even pages
        # the current version does not handle headers/footer varying along sections/chapters/...
        header_set = set((header for header in headers if len(headers[header]) / npages > 0.45 ))
        footer_set = set((footer for footer in footers if len(footers[footer]) / npages > 0.45 ))
        logging.info(f"headers: {header_set}")
        logging.info(f"footers: {footer_set}")
        # we type header/footer lines
        for header in header_set:
            for line in headers[header]:
                line.kind = Header()
                line.where = 'header'
        for footer in footer_set:
            for line in footers[footer]:
                line.kind = Footer()
                line.where = 'footer'
        # we can use elements in header_set/footer_set to identify the one carrying the page number
        def find_page_numbering(lines,template):
            distincts = set(line.content for line in lines)
            if len(distincts) > 0.8 * len(lines):
                logging.info(f"potential page number in {template} {len(distincts)} lines out of {len(lines)}")
                info = defaultdict(list)
                for line in lines:
                    for i,num in enumerate(re.findall(r'(\d+)',line.content)):
                        num = int(num)
                        if i not in info or info[i][-1][0] < num:
                            info[i].append((num,line.page))
                logging.info(f"{info=}")
                for i in info:
                    if len(info[i]) == len(distincts):
                        logging.info(f"page number in {template} as {i}th number")
                        for logical_pageid,physical_pageid in info[i]:
                            self.page_mapping[logical_pageid] = physical_pageid
                            self.pages[physical_pageid].page_number = logical_pageid
        for header in header_set:
            find_page_numbering(headers[header],header)
        for footer in footer_set:
            find_page_numbering(footers[footer],footer)                           
        # we can use elements in header_set/footer_set to potentially identify top and bottom coordinates
        if header_set:
            top = 0
            for header in header_set:
                top = max(top, np.median(np.array([line.y1 for line in headers[header]])))
            logging.info(f"header sep line at {top}")
            for lines in headers.values():
                for line in lines:
                    if line.y1 <= top and type(line.kind) is not Header:
                        logging.info(f'Page {line.page} setting line {line.content} to header')
                        line.kind=Header()
                        line.where = 'header'

        if footer_set:
            bot = 10000
            for footer in footer_set:
                bot = min(bot,np.median(np.array([line.y0 for line in footers[footer]])))
            logging.info(f"bottom sep line at {bot}")
            for lines in footers.values():
                for line in lines:
                    if line.y0 >= bot and type(line.kind) is not Footer:
                        logging.info(f'Page {line.page} setting line {line.content} to footer')
                        line.kind=Footer()
                        line.where = 'footer'
        
    def filter_other_indexes(self):
        others = defaultdict(list)
        for line in self.collect(kind=OtherIndex):
            others[line.kind.intro].append(line)
        other_set = set((other for other in others if len(others[other]) > 10))
        logging.info(f"other indexes: {other_set}")
        # delete rare other-like entries
        for other in others:
            if other not in other_set:
                for line in others[other]:
                    line.kind = None
        # handle other entries to retrieve them on pages
        if self.page_mapping:
            for other in other_set:
                for line in others[other]:
                    if type(line.kind) is not OtherIndex: continue
                    page_number = line.kind.page
                    if page_number in self.page_mapping:
                        page_id = self.page_mapping[page_number]
                        for xline in self.pages[page_id].lines:
                            if xline.check_with_other_index(line):
                                break

    def process_indexes (self,indexes):
        for line in indexes:
            self._tdm[line.kind.index].append(line)
            self._tdm_light[line.kind.numbering].append(line)

    def is_section_in_index (self, section):
        if self._tdm:
            return section.index in self._tdm
        else:
            return True
    
    def check_and_filter_sections(self):
        if not hasattr(self, 'deleted_sections'):
            self.deleted_sections = []
        else:
            for line in self.deleted_sections:
                logging.info(f'Page {line.page} potential section {line} already deleted')
        self._tdm = defaultdict(list)
        self._tdm_light = defaultdict(list)
        self.process_indexes(self.tdm)
        tdm = self._tdm
        tdm_light = self._tdm_light
        if not hasattr(self,'_tdm_light_extra'):
            self._tdm_light_extra = defaultdict(list)
        else:
            logging.info(f'{len(self._tdm_light_extra)} sections already checked')
        tdm_light_extra = self._tdm_light_extra
        sections = {}
        if tdm:
            logging.info(f"{len(tdm)} entries in ToC")
            missing = []
            extra = []
            nsections = 0
            for line in self.sections:
                numbering,title = index = line.kind.index
                nsections += 1
                if index not in tdm and numbering not in tdm_light_extra:
                    extra.append(line)
                elif numbering in tdm_light_extra:
                    sections[index] = line
                else:
                    sections[index] = line
                    tdm_light_extra[numbering].append(line)
            logging.info(f"{nsections} potential sections, {len(extra)} not in ToC")
            extra2 = []
            changed = set([])
            for line in extra:
                numbering,title = index = line.kind.index
                if (numbering not in tdm_light 
                    or not any(l.kind.index[1].startswith(title) for l in tdm_light[numbering])
                    ):
                    seqnum = line.kind.components
                    current = str(seqnum)
                    if seqnum.level > 1 and numbering not in tdm_light and numbering not in tdm_light_extra:
                        
                        def check_related (related,kind='sibling'):
                            # we test if the related section is in the ToC or has been previously added
                            # and check page numbers
                            if related in tdm_light_extra and tdm_light_extra[related][-1].page <= line.page:
                                logging.info(f"Page {line.page} {line} accepted as section because of {kind} {related} on page {tdm_light_extra[related][-1].page}")
                                sections[index] = line
                                tdm_light_extra[numbering].append(line)
                                return True
                            else:
                                return False
                            
                        other = str(seqnum.prev())  #  sibling (seqnum is modified when using navigation methods)
                        other2 = str(seqnum.prev()) # sibling of sibling
                        other3 = str(seqnum.up()) # parent
                        other4 = str(seqnum.up()) # grand-parent
                        seqnum.restore()
                        if current != other:
                            # if current has a potential previous brother, we test it
                            if check_related(other): continue
                            if other2 != other: 
                                # we accept a gap of 1 by testing sibling of sibling
                                if check_related(other2,kind='sibling-gap'): continue
                        else:
                            # otherwise we test its parent
                            if check_related(other3,kind='parent') or check_related(other3[:-1],kind='parent'): 
                                continue
                            # and if deep enough, we accept a gap by testing its grand-parent !
                            if seqnum.level > 3 and (check_related(other4,kind='grand-parent') or check_related(other4[:-1],kind='grand-parent')): 
                                continue
                        
                    logging.info(f"Page {line.page} section {line} not found in ToC: deleting")
                    self.deleted_sections.append(line)
                    line.kind = None
                    changed.add(line.page)
                    extra2.append(line)
                else:
                    sections[tdm_light[numbering][0].kind.index] = line
                    tdm_light_extra[numbering].append(line)
            for page_id in changed:
                logging.info(f"Updating page {page_id}")
                self.pages[page_id].detect_tables()
            if self.page_mapping:
                for index in tdm:
                    if index not in sections:
                        for line in tdm[index]:
                            page_number = line.kind.page
                            if page_number in self.page_mapping:
                                logging.info(f"missing section for ToC entry {line}")

    def models (self,kind=Section):
        return Counter(l.kind.model for l in self.collect(kind=kind)).most_common()
        
    def section_models (self):
        return self.models()
    
    def tdm_models (self):
        return self.models(kind=SecIndex)
    
    def other_index_models (self):
        return self.models(kind=OtherIndex)
    
    def other_models (self):
        return self.models(kind=Other)
    
    def display (self):
        logging.info("ToC models")
        print(self.tdm_models())
        logging.info("Section models")
        print(self.section_models())
        logging.info("Caption ToC models")
        print(self.other_index_models())
        logging.info("Caption models")
        print(self.other_models())
        logging.info("Sections")
        for section in self.sections:
            print(str(section))
        logging.info("Captions")
        for caption in self.captions:
            print(str(caption))
        logging.info(f"Displaying all pages")
        for page in self.pages:
            logging.info(f"Page {page.id} display")
            page.display()
            
    def display_html_overview(self):
        html_display = []
        models = [("TOC Models",self.tdm_models()),
                  ("Section Models",self.section_models()),
                  ("Caption TOC Models",self.other_index_models()),
                  ("Caption models",self.other_models())]
        sections = [section for section in self.sections]
        captions = [section for section in self.captions]
        
        def make_table(lst):
            table = []
            styles = [("style","width:30%;"), ("border","1")]
            ths = [make_html("Model", "th"), make_html("Frequency", "th")]
            table.append(make_html("\n".join(ths), "tr", parent=True))
            for t in lst:
                td1,td2 = make_html(t[0],"td"), make_html(str(t[1]),"td")
                tr = [td1,td2]
                table.append(make_html("\n".join(tr), "tr", parent=True))
            table = make_html("\n".join(table), "table", parent=True, attval=styles)
            return table
        
        def make_list(lst):
            ul = []
            for t in lst:
                txt = t.content
                attval=[("style",HTML_COLORS[t.kind.color])]
                li = make_html(txt, tag="li", attval=attval)
                ul.append(li)
            ul = make_html("\n".join(ul),"ul",parent=True)
            return ul
        
        for m in models:
            title, lst = m
            html_display.append(make_html(title, tag="h2"))
            html_display.append(make_table(lst))
            
        html_display.append(make_html("Sections", tag="h2"))
        html_display.append(make_list(sections))
        html_display.append(make_html("Captions", tag="h2"))
        html_display.append(make_list(captions))
        html = make_html(text="\n".join(html_display), tag="html",parent=True)
        return html

    def to_html(self,path="./pages", pdf=None):
        os.makedirs(path,exist_ok=True)
        with open(f"{path}/overview.html", "w", encoding="utf8", newline="\n") as inf1:
                inf1.write(self.display_html_overview())
        for i, page in enumerate(self.pages):
            with open(f"{path}/page_{str(i+1)}.html", "w", encoding="utf8", newline="\n") as inf:
                inf.write(page.display_html())

    def get_section_lines(self, index = None):
        '''retrieves all the lines that occur under each section in the form of a list of dicts.
        if index is given, only the section with the index is returned'''
        sections = list(self.sections)
        section_dicts = []
        for i, section in enumerate(sections):
            if i == len(sections)-1 :
                break
            next_section = sections[i+1]
            section_page = self.pages[section.page]
            page_lines = section_page.lines
            section_index = page_lines.index(section) 
            if next_section in page_lines:
                next_section_index = page_lines.index(next_section)
                section_text = [{"page":section.page,"lines":page_lines[section_index+1:next_section_index]}]
            else:
                next_pages = self.pages[section.page+1::]
                section_text = [{"page":section.page, "lines":page_lines[section_index+1::]}]
                for next_page in next_pages:
                    if next_section not in next_page.lines:
                        section_text.append({"page":self.pages.index(next_page), "lines": next_page.lines})
                    else:
                        next_section_index = next_page.lines.index(next_section)
                        last_text = next_page.lines[0:next_section_index]
                        if last_text:
                            section_text.append({"page":self.pages.index(next_page), "lines":last_text})
                        break
            section_dicts.append({"section_object": section, "content":section.content,"index":i, "section_lines":section_text})
        if index is None:
            return section_dicts
        else:
            return section_dicts[index]
    
    def show_paras(self,section_index):
        section_dict = self.get_section_lines(index=section_index)
        if len(section_dict["section_lines"]) == 1 :
            section_lines = section_dict["section_lines"][0]["lines"]
        else:
            section_lines = [line for pagedic in section_dict["section_lines"] for line in pagedic["lines"] if type(line.kind) is not Footer and type(line.kind) is not Header ]
        blocks = group_by_kind(section_lines)
        text_blocks = [b for b in blocks if b["kind"] == None ]
        print("found",len(text_blocks), "continuous text blocks in this section")
        all_paras = []
        for b in text_blocks:
            #print("block",text_blocks.index(b))
            block_paras = find_paragraphs_in_block(b)
            all_paras.append(block_paras)
        
        print("paragraphs".upper())
        for block_paras in all_paras:
            for paras in block_paras:
                for para in paras:
                    print("type : ", para["type"])
                    if para["type"] == "normal":
                        print(clean_para(para["para"]))
                    else:
                        for line in para["para"]:
                            print(line)
                print()
                            
    def get_normal_paras(self,index):
        section_dict = self.get_section_lines(index=index)
        if len(section_dict["section_lines"]) == 1 :
            section_lines = section_dict["section_lines"][0]["lines"]
        else:
            section_lines = [line for pagedic in section_dict["section_lines"] for line in pagedic["lines"] if type(line.kind) is not Footer and type(line.kind) is not Header ]
        blocks = group_by_kind(section_lines)
        text_blocks = [b for b in blocks if b["kind"] == None ]
        all_paras = []
        for b in text_blocks:
            block_paras = find_paragraphs_in_block(b)
            all_paras.append(block_paras)
        all_normal_paras = [filter_normal_paras(para) for paras in all_paras for para in paras]
        flattened = [txt for lst in all_normal_paras for txt in lst]
        return flattened
            
    
        
        
    
            
        